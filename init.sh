#!/bin/bash

echo "This generates the absolute minimum configuration information"
echo "to start using Conductor...for advanced features edit the generated file"

prompt="Please select a context that defines the cluster to use:"
unset Options i
while IFS= read -r -d $'\n' f; do
  Options[i++]=$f
done < <(kubectl config get-contexts -o name)

PS3="$prompt "
select kubeContext in "${Options[@]}" "Quit" ; do
    if (( REPLY == 1 + ${#Options[@]} )) ; then
        exit

    elif (( REPLY > 0 && REPLY <= ${#Options[@]} )) ; then
        echo  "You picked $kubeContext which is file $REPLY"
        break

    else
        echo "Invalid Selection. Try another one."
    fi
done

deployPath="conductor-k8-$kubeContext"

export CLUSTER_NAME=$kubeContext
echo "Enter the BASE_DNS for Kubernetes Cluster:"
echo "access will be: conductor.$kubeContext.BASE_DNS"
read -p "BASE_DNS: " BASE_DNS
export BASE_DNS=$BASE_DNS
echo "Repository Conductor Templates are deployed to"
echo "must be accessible in all namespaces in the cluster"
echo "SemVer or Static Tag should be used"
read -p "Conductor Repository & tag {repo:tag}: " CONDUCTOR_REPO

rm -rf $deployPath.yaml; envsubst < conductor-k8.yaml > $deployPath.yaml
