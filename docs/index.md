# Conductor

Agnostic & Recyclable Containerization Deployments

## Purpose

There are many different containerization orchestration technologies available in the market, and each have different design goals in mind, with variable roadmaps and features. Unfortunately, while the market trends towards data ownership and CI/CD it becomes more challenging to select an orchestration technology that won't hamper or lock in to a specific vendor. Additionally, 12 Factor application design is becoming more common-place amongst legacy applications with no simple deployment upgrade path when companies standardize on containerization. **Conductor** removes these challenges by providing a layer between container orchestration and 12 Factor App design in a way that supports Legacy to 12 Factor transitions while applying the best known and accepted deployment practices irrespective of what the underlying containerization orchestration technology is.

Fundamentally Conductor is to Deployment Pipelines what Kanban is to Agile. In Deployment Pipelines items are pushed through the pipe until they appear in production with multiple gates or gatekeepers along the path. Conductor uses a Pull approach where items are automatically progressed when ready.

Its a subtle change in logic, but empowers high quality application principles and makes 12 Factor simpler to manage.

## Features

- Infrastructure agnostic, describe your application needs and run on any infrastructure through templates
- Base templates are provided for all major infrastructure players demonstrating the sample application and best practices
- Recognises the infrastructure its deployed on
- Secure management of secret information using Transcrypt
- Git Flow & Git Trunk design & release style

## Supported technologies

- Kubernetes
- TODO: Helm Support
- TODO: Kontena
- TODO: Mesos
- TODO: Spinnaker

## Installation & Usage

*Conductor* is provided as stripped down Docker Image. It requires a 'conductor.yaml' file along with templates for the specific backend container orchestration technology to be provided. Base templates for each supported technology are available on GitHub.

The template for your backend container orchestration platform would be modified if necessary to suit then built into a Docker Container and deployed. Finally WebHooks are provided to Conductor whenever a new namespace is required.

- Updating application configuration is as simple as changing the 'ConfigMap' conductor uses.
- Updating releases is as simple as pushing to the latest tag and enabling 'releaseTracking' in Conductor for the applications to be kept current.
- (Future) Built Docker Image should just container templates & secret keys for application interations
