# Root Struture

```
apps:
  exampleGroup:
    group: {}
    exampleApp: {}
```

*Apps* are structured under groups, while this may not make sense with a small number of microservices its a critical organization approach for Service Oriented Applications (SOA) where microservices are part of an SOA

As such its possible to use conductor with any level of complexity in application releationships, while keeping isolation and deployment in a layered security approach.

## Group Template

```
apps:
  exampleGroup:
    group:
      depends:
        - itStarts
        - itShouldStart.something
      namePrefix: another
      OmitFromDeploy:
        - stub
      serviceAccount:
        name: specificOne
        roleRef: view
```

While flexible, naming of groups is important as the resulting auto-generated app, domain and Docker registry names will by default incorporate the GroupName (can be veto'd if required)

- Template files being by design generic in nature should have the same name as the group. As such all apps listed under this group will then use the same template for generation.
- Alternatively if a template file matches an application name then it will be used for the application instead of the group one.

### Depends

```
apps:
  exampleGroup:
    group:
      depends:
        - itStarts
        - itShouldStart.something
```

This is a cross linking between application groups. More specific to the git-flow / trunk approach Conductor assists in implementing for Continuous Deployment.

Listings here specifically enforce that groups & / or applications are also deployed into the namespace whenever this group is deployed.

<!-- TODO: Additionally, adding a depends ensures that the applications in this group will not be deployed until these dependent applications succeed in receiving traffic. -->

### OmitFromDeploy

```
apps:
  exampleGroup:
    group:
      OmitFromDeploy:
        - stub
```

This is a list of *Apps* within this group that will be blacklisted from deploying in all environments.

### serviceAccount

```
apps:
  exampleGroup:
    group:
      serviceAccount:
        name: specificOne
        roleRef: view
```

Following the Kubernetes serviceAccount principles, if a custom serviceAccount is required for permissions / group access this would create it using the 'roleRef' of a matching ClusterRole. The default account if this is omitted is 'default' with no minimal to no permissions

## App Template

```
apps:
  exampleGroup
    exampleApp:
      vetos: {}
      imageTag: '1.1.x'
      depends: {}
      labels: {}
      ALB: 'enabled'
      ingress: {}
      environment: {}
      resources: {}
      HA: {}
      probes: {}
      terminationGracePeriodSeconds: '30'
      env: {}
```

Only mandatory tag is 'imageTag'

### vetos

```
apps:
  exampleGroup
    exampleApp:
      vetos:
        name: example-app
        enableProfiler: 'true'
        serviceAccount: customized
        autoDeploy: 'false'
```
- *name*: is used for DNS registration & image retrieval from the container registry
- *enableProfiler*: is a hard override to settings applied from the namespace, this will force enable or disabled irrespective of namespace settings
- *serviceAccount*: should an account be required that isn't the group serviceAccount or 'default' i.e. another group's account. It can be overriden here
- *autoDeploy*: when set to 'false' it disables any automatic deployments for the application.

### imageTag

```
apps:
  exampleGroup:
    exampleApp:
      imageTag: '1.1.x'
```

This is the only truly 'opinionated' approach in Conductor, SemVer is getting more traction for structured releases and provides an excellent approach to versioning where if used and autoDeploy is enabled then Conductor will automatically deploy relevant versions into their respective environments.

### depends

```
apps:
  exampleGroup
    exampleApp:
      depends:
        - exampleApp2
```

Following the same approach as group:depends apps listed here will be deployed prior to this one.

<!-- And this application will not be deployed until these are capable of receiving traffic -->

### labels

```
apps:
  exampleGroup:
    exampleApp:
      labels:
        exampleLabel: 'value'
        # app:    autocompleted ~ exampleApp
        # tier:   autocompleted ~ exampleGroup
        # track determines how often this environment promotes its candidate release
        # track:  autocompleted ~ namespace.releaseFrequency || can override
        # releases are candidate style i.e. canary -> stable
        # release: autocompleted ~ canary or stable
        #
        # Can add any other labels necessary in here for this application
      # ALB: 'enabled'
```

Any labels can be used to help identify the applications & services. The following are reserved labels that are generated by conductor based on the provided yaml to accurately identify what's deployed where. Text following the '~' sign in the based on the above yaml

- *app*: autocompleted ~ exampleApp in above example
- *tier*: autocompleted ~ exampleGroup
- *track*: autocompleted ~ namespace.releaseFrequency || *can override*. Track determines how often this environment promotes its candidate release
- *release*: autocompleted ~ canary or stable. releases are candidate style i.e. canary -> stable

Hotfix isn't a label as we strongly suggest following SemVer for deployments, and as seen all applications have the ability to promote through the 'canary' state. Effectively under candidate based releases hotfixes are just releases that have been accelerated through the canary phase.

promotion would happen through a webhook

### ingress

```
apps:
  exampleGroup:
    exampleApp:
      ingress:
        # namespace.ingress.* determines which ingress option is available for this application
        # these are additional settings specific to application profile
        # host: autocompleted ~ customport-exampleApp.exampleGroup.namespace.fqdn_base
        additionalHosts:
          # FORMAT: Full DNS must be specified : path
          # TODO Nested Paths
          # - path
          #   hostname
          #   ports
          #     - 8080
          'something.somewhere': '/somewhere-else'
        vetos:
          path: '/override'
        # exposeProduction: 'true'
```

May be restructured for clearer process.

### environment

```
apps:
  exampleGroup:
    exampleApp:
      environment:
        itStarts: awesome
```

Everything under environment will be exposed to the application. Should environmental names be the same as those specified in 'namespaces' then this will take priority.
