# Root Structure

```
namespaces
  mandatory: {required}
  dev:
  ha:
  perf:
  prod:
```

Namespaces follow the traditional release process where applications are deployed and tested either automated or manual and must pass a series of required tests prior to advancing to the next phase of the deployment pipeline.

Flow would be:
** dev (non-ha) -> ha -> perf -> prod {canary & stable} **

Or in more detail:

*Devlike*

- dev: is a non high availability environment to ensure that application meets test coverage & passes required tests.
- ha: is a high availability environment where two instances of the application run. Tests are executed through a load balancer, and any discovery logic would also be tested.

*ProdLike*

- perf: is a performance testing environment where its a replication of production, auto-scaling rules would apply.
- prod: is the actual production environment, the concept of *canary* releases is supported for partially loading traffic prior to full roll-out.

## Mandatory

```
namespaces:
  mandatory:
    HA:
      minReplicas: '2'
      maxReplicas: '2'
      autoScaler:
        targetCPUUtilizationPercentage: '80'
```
<!-- TODO: Redundant auto-scaling on ingress should always be available -->

## Environment Template

Applies to all environmental types (dev, ha, perf, prod). {Custom Names to be applied}

```
namespaces:
  dev:
    OmitFromDeploy: {}
    resources: {}
    environmental: {}
    ingress: {}
```

### OmitFromDeploy

```
namespaces:
  dev:
    OmitFromDeploy:
      - stub
      - *.stub
      - stub.stubbed
```

This is a list of *Groups* and *Apps* within groups that will be blacklisted from deploying in the environment.

1. stub         = the entire Group 'stub' *Group* of *Apps* to be blacklisted
2. *.stubbed    = the 'stubbed' App in any Group
3. stub.stubbed = only the 'stubbed' App in the Group 'stub'

Priority also follows this order, where Group > WildCard > Named App

### environmental

```
namespaces:
  dev:
    environmental:
      HADEV: 'true'
      PRODLIKE: 'true'
      enableProfiler: 'true'
      NRTAG: 'PERFORMANCE' # Used with enableProfiler to add custom tags to environments
      INGRESS_ALL_ROUTES: 'true' # allows development routes to be force exposed in 'PRODLIKE' environments
      JAVA_OPTS: ""
      {any others you require}
```

Environment naming approaches don't have to follow the outline suggested here, as Environment types are controlled via variables in this part of the API. Everything under this API tag is converted to environmental variables in the deployment. If specified again at the *App* level then variables will be overriden by *App* specifications

**dev**

 - (non-ha)
 - (minimum resource allocation)
 - (non-liveliness probing)

**ha @ HADEV=true**

 - (ha)
 - (minimum resource allocation)
 - (non-liveliness probing)

**perf & prod @ PRODLIKE=true**

 - (ha)
 - (maximum resource allocation)
 - (livenessProbe for restarting failed containers)

If HADEV & PRODLIKE are both 'true' then HADEV autoscaling rules will override PRODLIKE rules resulting in a replica of 'HA' environment but with PRODLIKE settings

**enableProfiler**
Is a special tag, that when defined enables your defined profiling logic for all applications in the namespace. i.e. NewRelic or SemaText for Java applications

### Ingress

```
namespaces:
  dev:
  ingress:
    tls:
      # SSL external termination point
      # cluster: 'true'
      # elb: 'true'
      # ACM: ''
    nginx: 'true'
    # istio: 'true'
```

Conductor creates an ingress controller for every deployed namespace, this ensures that access scope is locked to within the namespace, and choices are available of where SSL termination should occur for services. Selection is simple, just enable the one to use, and leave the others disabed / omimtted

Choices are also available of which controller should handle the ingress, nginx / istio

### Resources

```
namespaces:
  dev:
    resources:
      container:
        defaultmax:
          cpu: 250m
          memory: 1Gi
        defaultmin:
          cpu: 100m
          memory: 1Gi
```

It is important that this is always specified in namespaces as these are the default applied container limits when not provided in the application. The benefit of just assignign the defaults is that applications can specify their own requirements when necessary.

If more fine-grained control is required then its possible to use the following options.

```
namespaces:
  dev:
    resources:
      container:
        max:
         cpu: 500m
         memory: 1Gi
        min:
         cpu: 50m
         memory: 10Gi
```
