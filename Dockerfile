FROM gcr.io/taylored-technology/conductor:latest

MAINTAINER Keidrych Anton-Oates <keidrych@tayloredtechnology.net>

# mount CONFD Configuration files
ADD templates/ /etc/confd/templates/

ADD conductor.yaml /deploy/

# Specify default encryption secret for webhook transactions
ARG HOOKSECRET=15ed0cf8669bc4ea2461be46fba83002578ed72e

# location of conductor cluster configuration & deployment file
# ENV CONF=/deploy/conductor.yaml

# enable DRY_RUN if just wanting to generate output files for kubectl usage
# mount /app directory to disk
# ENV DRY_RUN=true
