# Template Management & Teardown Service

## Purpose
This is the key enabler of deploying services in namespaces. Effectively templates are created using GoLang's templating approach allowing for conditional logic and then values are interpreted via environmental variables. Its k8's designed.

Gets built into a Docker Container for deployment

## Multi-Stage project
Watch this space ><

- POD_NAME
- POD_NAMESPACE

Transcrypt used for secret management
trascrypt directory secrets /transcrypt
- Each app group has a specific transcrypt key
- Should be pushed in via secret management as part of kubernetes deploy
