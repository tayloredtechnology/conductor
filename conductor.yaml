apps:
  # group of microservices required to be deployed together for functionality
  #
  # App name format: {GroupName}-{AppName}
  # if template file is the same as the group name it will apply to all templates by default
  # otherwise templates are matched to app name as specified here
  exampleGroup:
    # Group Specific Information
    group:
      # Group based dependencies
      # drill down into specific app in group if desired
      depends:
        - itStarts
        - itShouldStart.something
      # Prefix to apply to all app-names to keep naming DRY
      namePrefix: another
      OmitFromDeploy:
        - stub
      serviceAccount:
        name: specificOne
        roleRef: view
    # App Information
    exampleApp:
      vetos:
        # Name is used for DNS registration & image retrieval from the container registry
        name: example-app
        # Generic profiler override for application
        enableProfiler: 'true'
        # select a specific serviceAccount to run under different to the group required one
        serviceAccount: customized
        # Enable if wanting to turn off automatic deployments
        autoDeploy: 'false'
      # ImageTag's will be auto-deploy capable if they follow
      # -> SemVer X.Y.Z or
      # -> Fixed Labels
      imageTag: '1.1.x'
      # Group scoped dependency management
      # Will not deploy this app until these are available
      depends:
        - exampleApp2
      labels:
        # app:    autocompleted ~ exampleApp
        # tier:   autocompleted ~ exampleGroup
        # track determines how often this environment promotes its candidate release
        # track:  autocompleted ~ namespace.releaseFrequency || can override
        # releases are candidate style i.e. canary -> stable
        # release: autocompleted ~ canary or stable
        # hotfix is force pushed into stable, this shows the tag that should be applied
        # its automatically removed when next candidate is promoted
        # hotfix: autocompleted ~ date / release tagging
        #
        # Can add any other labels necessary in here for this application
      # ALB: 'enabled'
      ingress:
        # namespace.ingress.* determines which ingress option is available for this application
        # these are additional settings specific to application profile
        # host: autocompleted ~ customport-exampleApp.exampleGroup.namespace.fqdn_base
        additionalHosts:
          # FORMAT: Full DNS must be specified : path
          # TODO Nested Paths
          # - path
          #   hostname
          #   ports
          #     - 8080
          'something.somewhere': '/somewhere-else'
        vetos:
          path: '/override'
        # exposeProduction: 'true'
      environment:
        itStarts: awesome
      resources:
        # Namespace has maximum limits enforced on it, as such its not necessary to specify these, until app requirements are known
        # Container limits are ignored for now as pods are more critical & current behaviour is almost 1Pod = 1Container (Kubernetes)
        #
        # A production environment should always have min/max allocations, devtest doesn't technically need 'max' values so the design flow
        # is such that 'max' values apply to PROD like environments and 'min' values should apply to devtest environments.
        # This solves the % of environment challenge where prod or devtest configuration may be isolated from eachother
        max:
          # PROD Like environments
          cpu: 500m
          memory: 1Gi
        min:
          # devtest environments
          cpu: 300m
          memory: 1Gi
      # Only selective environments apply HA settings to ensure rapid identification of defects in relevant environments
      # for the development pipeline following normal pipeline flows for Continous Deployment
      # dev (non-ha) -> ha -> perf -> prod {canary & stable}
      # if 'name' of environment follows above convetion its automatically applied, if not then specifying the environmental
      # variable 'HADEV=true' enables the HA such that any environment can be overriden / customized
      HA:
        minReplicas: '1'
        maxReplicas: '2'
        maxUnavailable: "30%"
        maxSurge: "10%"
        autoScaler:
          targetCPUUtilizationPercentage: '75'
      probes:
        healthcheck: '/override'
        # times in seconds
        minStability: '30'
        minReady: '1'
        minBoot: '10'
        pollingInterval: '10'
        port: '80'
        timeout: '1'
        # percentage of stability before restart should be initiated
        rebootFactor: '200%'
      terminationGracePeriodSeconds: '30'
      enableProfiler: 'true'
      env:
        OPENSHIFT_KUBE_PING_LABELS: "tier=air-provider-sabre"

# specific cluster services / settings that should be configured
cluster:
  mandatory:
    autoscaler:
      # CLUSTER-{group name} : max allowed
      workers: '6'
  # datadog:
  #   apiToken: 'e1d0d3a7c0d810e59f497d59634c682a'

namespaces:
  # Meta versions of namespaces and specific rules that should be applied to them. i.e. stg-*
  # mandatory will be activated in all namespaces with these settings
  mandatory:
    HA:
      minReplicas: '2'
      maxReplicas: '2'
      autoScaler:
        targetCPUUtilizationPercentage: '80'
  stg:
    OmitFromDeploy:
      - stub
    resources:
      # Pod limits are ignored for now as pods are more critical & current behaviour is almost 1Pod = 1Container
      # pod:
      #   max:
      #     cpu: 500m
      #     memory: 1Gi
        # minimum settings will be enforced through the namespace such that anything not requsting > than these would be denied
        # min:
        #   cpu: 50m
        #   memory: 10Gi
      # Design intention is such that the containers should be automatically allocated when services don't know what their requirements
      # are. As such each environment would enforce. App level resource overrides are used when we understand what the application actually
      # requires in devtest & production environments.
      #
      # A production environment should always have min/max allocations, devtest doesn't technically need 'max' values so the design flow
      # is such that 'max' values apply to PROD like environments and 'min' values should apply to devtest environments
      container:
        defaultmax:
          cpu: 250m
          memory: 1Gi
        defaultmin:
          cpu: 100m
          memory: 1Gi
        # max:
        #   cpu: 500m
        #   memory: 1Gi
        # min:
        #   cpu: 50m
        #   memory: 10Gi
    # everything in 'environmental' is exposed as environmental variables
    environmental:
      # Environment types:
      # dev
      #   - (non-ha)
      #   - (minimum resource allocation)
      #   - (non-liveliness probing)
      #  ha @ HADEV=true
      #   - (ha)
      #   - (minimum resource allocation)
      #   - (non-liveliness probing)
      # perf & prod @ PRODLIKE=true
      #   - (ha)
      #   - (maximum resource allocation)
      #   - (livenessProbe for restarting failed containers)
      # HADEV: 'true'
      # PRODLIKE: 'true'
      # Enable NewRelic / DataDog profiling on this namespace template
      enableProfiler: 'true'
      NRTAG: 'PERFORMANCE'
      # INGRESS_ALL_ROUTES allows development routes to be force exposed in 'PRODLIKE' environments
      JAVA_OPTS: ""
    ingress:
      tls:
        # SSL external termination point
        cluster: 'true'
        # elb: 'true'
        # ACM: ''
      nginx: 'true'
      # istio: 'true'
