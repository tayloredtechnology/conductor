apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-template
  namespace: NAMESPACED
data:
  proxy-connect-timeout: "15"
  proxy-read-timeout: "600"
  proxy-send-timeout: "600"
  hsts-include-subdomains: "false"
  body-size: "64m"
  server-name-hash-bucket-size: "256"
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nginx
  namespace: CONDUCTED
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: nginx-NAMESPACED
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: nginx
subjects:
  - kind: ServiceAccount
    name: nginx
    namespace: NAMESPACED
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-ingress
  namespace: NAMESPACED
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: http
    service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled: 'true'
    {{if exists "/ingress/tls/ACM"}}
    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: "{{getv "/ingress/tls/ACM"}}"
    service.beta.kubernetes.io/aws-load-balancer-ssl-ports: https
    {{end}}
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 80
      name: http
    - name: https
      port: 443
      {{if getenv "INGRESS_TLS_CLUSTER"}}
      targetPort: 443
      {{else}}
      targetPort: 80
      {{end}}
  loadBalancerSourceRanges:
    - 203.160.2.0/23
    {{if getenv "PRODLIKE"}}
    {{else}}
    - 14.201.234.162/32
    {{end}}
    - 52.62.252.27/32
    - 52.63.57.132/32
    - 52.63.151.183/32
    - 13.54.14.221/32
    - 52.62.76.132/32
    - 52.63.1.140/32
    - 13.55.181.117/32
    - 52.62.211.147/32
  selector:
    k8s-app: nginx-ingress-lb
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-ingress-controller
  namespace: NAMESPACED
  labels:
    effective.namespace: NAMESPACED
spec:
  revisionHistoryLimit: 2
  template:
    metadata:
      labels:
        k8s-app: nginx-ingress-lb
    spec:
      # Affinity selected based on avaiable node pool or not master pool
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: node-role.kubernetes.io/master
                    operator: DoesNotExist
      terminationGracePeriodSeconds: 60
      serviceAccountName: nginx
      containers:
        - name: nginx-ingress-controller
          image: gcr.io/google_containers/nginx-ingress-controller:0.9.0-beta.5
          imagePullPolicy: IfNotPresent
          readinessProbe:
            httpGet:
              path: /healthz
              port: 18080
              scheme: HTTP
          livenessProbe:
            httpGet:
              path: /healthz
              port: 18080
              scheme: HTTP
            initialDelaySeconds: 10
            timeoutSeconds: 5
          args:
            - /nginx-ingress-controller
            - --default-backend-service=NAMESPACED/default-http-backend
            - --default-ssl-certificate=NAMESPACED/default-tls-certificate
            - --publish-service=NAMESPACED/nginx-ingress
            - --ingress-class=NAMESPACED
            - --watch-namespace=NAMESPACED
          # Use downward API
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - containerPort: 80
            - containerPort: 443
          volumeMounts:
            - name: tls-dhparam-vol
              mountPath: /etc/nginx-ssl/dhparam
            # Can modify entire nginx configuration file if necessary
            # download /etc/nginx/template/nginx.tmpl edit and re-upload with
            # kubectl create configmap nginx-template --from-file=nginx.tmpl=./nginx.tmpl
            # - name: nginx-template-volume
            #   mountPath: /etc/nginx/template
            #   readOnly: true
      volumes:
        - name: tls-dhparam-vol
          secret:
            secretName: default-tls-dhparam
        # - name: nginx-template-volume
        #   configMap:
        #     name: nginx-template
        #     items:
        #     - key: nginx.tmpl
        #       path: nginx.tmpl
---
kind: HorizontalPodAutoscaler
apiVersion: autoscaling/v1
metadata:
  name: nginx-ingress-autoscaler
  namespace: NAMESPACED
  labels:
    app: nginx-ingress-lb
spec:
  scaleTargetRef:
    apiVersion: apps/v1beta1
    kind: Deployment
    name: nginx-ingress-controller
  # Requires a minimum of 2 for loadbalancing with Ingress endpoint
  minReplicas: {{getv "/HA/minReplicas" "2"}}
  maxReplicas: {{getv "/HA/maxReplicas" "2"}}
  {{range gets "/HA/autoScaler/*"}}
  {{base .Key}}: {{.Value}}
  {{end}}
