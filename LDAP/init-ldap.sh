#!/bin/bash

prompt="Please select a context that defines the cluster to use:"
unset Options i
while IFS= read -r -d $'\n' f; do
  Options[i++]=$f
done < <(kubectl config get-contexts -o name)

PS3="$prompt "
select kubeContext in "${Options[@]}" "Quit" ; do
    if (( REPLY == 1 + ${#Options[@]} )) ; then
        exit

    elif (( REPLY > 0 && REPLY <= ${#Options[@]} )) ; then
        echo  "You picked $kubeContext which is file $REPLY"
        break

    else
        echo "Invalid Selection. Try another one."
    fi
done

echo "Generating global integration & configurations to layer ontop of tectonic"

# Enable LDAP
echo "generating ... tectonic-ldap integration..."
kubectl --context $kubeContext --namespace tectonic-system get configmap tectonic-identity -o yaml > LDAP/tectonic-identity.yaml
sed -i '/enablePasswordDB/r LDAP/ldap-config.txt' LDAP/tectonic-identity.yaml

echo "deleting tectonic identity pod to enforce loading LDAP configuration"
kubectl --context $kubeContext --namespace tectonic-system delete $(kubectl --context $kubeContext --namespace tectonic-system get pods -o name | grep identity)
